/* jshint node: true */
"use strict";

var fs = require('fs');
var spawn = require('child_process').spawn;

var rar = function(filename)
{
	var thisrar = this;
	this.filename = filename;
	var unrar = spawn('7z', ['l', '-slt', filename]);
    var strout = '';
	unrar.stdout.on('data', function(data)
	{
		strout += data.toString();
	});

	unrar.stdout.on('close', function()
	{
	    thisrar.contents = [];
        var lines = strout.split('\n');
        lines.forEach(function(line)
        {
            var path = line.match(/^Path = (.*)/);
            if(path)
            {
                thisrar.contents.push(path[1]);
            }
        });
		if(thisrar.cb)
		{
			thisrar.cb(thisrar.contents);
		}
	});

	unrar.stderr.on('data', function(data)
	{
		console.log(data.toString());
	});
};

module.exports = rar;

rar.prototype.getFiles = function(cb)
{
	if(this.contents)
	{
		cb(this.contents);
	}
	else
	{
		this.cb = cb;
	}
};

rar.prototype.extractFile = function(file, destination, cb)
{
	var unrar = spawn('7z', ['e', this.filename, file, '-o' + destination]);
	unrar.stdout.on('data', function(data)
	{
		console.log(data.toString());
	});
	unrar.stderr.on('data', function(data)
	{
		console.log(data.toString());
	});
	unrar.on('exit', function(code)
	{
		cb();
	});
    console.log(file);
};
