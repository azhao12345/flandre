"use strict";

var spawn = require('child_process').spawn;

module.exports = function(filename, cb)
{
	console.log(filename);
	console.log('splawning encoders');
	var flac = spawn('flac', ['-c', '-d', filename + '.flac'], { cwd: '/tmp', env: process.env });
	var lame = spawn('lame', ['-V4', '-', filename + '.mp3'], { cwd: '/tmp', env: process.env });
	
	flac.stdout.on('data', function(data)
	{
		lame.stdin.write(data);
	});
	flac.stderr.on('data', function(data)
	{
		console.log(data.toString());
	});
	
	flac.stdout.on('close', function()
	{
		lame.stdin.end();
	});
	lame.stdout.on('data', function(data)
	{
		console.log(data.toString());
	});

	lame.on('exit', cb);
};
