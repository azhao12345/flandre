/* jshint node: true */
"use strict";

var fs = require('fs');
var express = require('express');
var config = require('./config');
var rar = require('./7zip');
var encode = require('./encoder');


///////////////////////////////////////////////
//web server part
var app = express();

//Get request body
app.use(function(req, res, next)
{
	var data = '';
	req.setEncoding('utf8');
	req.on('data', function(chunk)
	{
		data += chunk;
	});

	req.on('end', function()
	{
		req.body = data;
		next();
	});
});

//disable 2 minute timeout
app.use(function(req, res, next)
{
	res.socket.setTimeout(0);
	next();
});



//gets a list of available rar files in the folder
var rarFiles = [];
app.get('/files', function(req, res)
{
	res.send(JSON.stringify(rarFiles));
});

var rarFileContents = {};
app.post('/fileContents', function(req, res)
{
	rarFileContents[req.body] = rarFileContents[req.body] || new rar(config.fileDir + req.body);
	rarFileContents[req.body].getFiles(function(files)
	{
		res.send(JSON.stringify(files));
	});
});


var encodeList = {};
app.post('/extractAndEncode', function(req, res)
{
	//TODO clean up this mess
	var body = JSON.parse(req.body);

	//check if it's already being processed/done
	if(encodeList[req.body] && encodeList[req.body].encoding)
	{
		console.log('already encoding');
		var cb = encodeList[req.body].encodeFinished || function(){};
		encodeList[req.body].encodeFinished = function() { res.send('finished'); cb(); };
		return;
	}
    
    var filename = body.file.substring(body.file.lastIndexOf('/') + 1, body.file.lastIndexOf('.'));

	//check if it exists already
	console.log(filename);
	fs.exists('/tmp/' + filename + '.mp3', function(exists)
	{
	    if(!exists)
	    {
	    
	    	encodeList[req.body] = {};
	    	encodeList[req.body].encoding = true;
	    	rarFileContents[body.rarFile].extractFile(body.file, '/tmp/', function()
	    	{
	    		console.log('extracted');
	    		encode(filename, function()
	    		{
	    			res.send('finished');
	    			if(encodeList[req.body].encodeFinished)
	    			{
	    				encodeList[req.body].encodeFinished();
	    				delete encodeList[req.body].encodeFinished;
	    			}
	    			encodeList[req.body].encoding = false;
	    			fs.unlink('/tmp/' + body.file.substring(body.file.lastIndexOf('/') + 1), function(){});
	    		});
	    	});
	    
	    }
	    else
	    {
	    	res.send('already exists');
	    }
	});
});

//serve encoded files directly
app.use('/mp3', express.static('/tmp/'));
//serve the static site
app.use('/', express.static('./www/'));

//listen for connections
app.listen(8080);



///////////////////////////////////////////
//filesystem setup
function updateFiles()
{
	fs.readdir(config.fileDir, function(err, files)
	{
		if(err)
		{
			console.log(err);
			process.exit(1);
		}
		rarFiles = files;
	});
}
//load the files initially
updateFiles();
//pereodically refresh the file list to populate newly added files
if(config.fileRefreshInterval)
{
	setInterval(updateFiles, config.fileRefreshInterval);
}
