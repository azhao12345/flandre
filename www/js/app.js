/* jshint browser: true */
/* jshint devel: true */
/* jshint -W097 */
/* global LZString: false */
"use strict";

window.onload = function()
{
    //load the filelist from the server
	var x = new XMLHttpRequest();
	x.onreadystatechange = function()
	{
		if(x.readyState == 4)
		{
            processAlbumList(JSON.parse(x.response));
		}
	};
	x.open('GET', '/files', true);
	//move this here because FF requires that
	x.send();
	
    //events for the player object
	var player = document.getElementById('player');
    //Play the next song when finished playing
    //do this because Safari cannot set this event via property
    var songEnded = function()
	{
		var queue = document.getElementById('queue');
		if(queue.children.length > 0)
		{
			var songElement = queue.children[0];
			if(songElement.className == 'ready')
			{
                playSongElement(songElement);
			}
			else
			{
                //the song is not encoded, clear the src so the song will start when the encode request
                //returns
				player.src = '';
			}
		}
		else
		{
            //there are no more songs left, clear the src so the new songs queued later will play
			player.src = '';
		}
	};
	player.addEventListener('ended', songEnded);

    //events for the playlist link
    var link = document.getElementById('link');
    //update the link when moused over to have the current playlist
    link.onmouseover = function()
    {
        var queue = document.getElementById('queue');
        
        //the first song in the playlist should be the currently playing one
        var songs = [document.getElementById('nowPlaying').textContent];
        //since queue.children is an HTMLCollection, we can't use use array comprehensions on it
        for(var i = 0; i < queue.children.length; i++)
        {
            songs.push(queue.children[i].textContent);
        }
        //update the link value with an encoded version of the array
        link.href = './?' + LZString.compressToEncodedURIComponent(JSON.stringify(songs));
    };

    //load playlist if passed in via url
    if(document.location.search)
    {
        var songs = JSON.parse(LZString.decompressFromEncodedURIComponent(window.location.search.substring(1)));
        songs.forEach(function(song)
        {
    		//put the song in the queue
            //we don't worry about encoding the song, because we expect the song to already 
            //have been encoded by whoever created the playlist
    		var queueElement = document.createElement('div');
    		queueElement.className = 'ready';
    		queueElement.textContent = song;
    		document.getElementById('queue').appendChild(queueElement);
        });
        //play the first song
        //to do this lazily, we will call the player onended funciton
        //TODO replace this with a named funciton somewhere so it isn't gross like this
        //player.onended();
        songEnded();
    }


    var nowPlaying = document.getElementById('nowPlaying');
    var queue = document.getElementById('queue');
    var queueVisible = false;
    nowPlaying.onclick = function()
    {
        if(queueVisible)
        {
            queue.style.display = 'none';
        }
        else
        {
            queue.style.display = 'block';
        }
        queueVisible = !queueVisible;
    };
};


function processAlbumList(names)
{
	var albumList = document.getElementById('albumList');
	for(var i = 0; i < names.length; i++)
	{
		if(names[i].match(/.*(rar|zip)/))
		{
			albumList.appendChild(createAlbumDiv(names[i]));
		}
	}
}

function createAlbumDiv(name)
{
	console.log(name);
	var albumDiv = document.createElement('li');
	albumDiv.textContent = name;

	albumDiv.onclick = function()
	{
		albumDiv.onclick = function(){};
		albumDiv.textContent += '...';
		var x = new XMLHttpRequest();
		x.onreadystatechange = function()
		{
			if(x.readyState == 4)
			{
                var response = JSON.parse(x.response);
				albumDiv.textContent = '>' + albumDiv.textContent;

                var albumSongsList = document.createElement('ul');
                albumDiv.appendChild(albumSongsList);

				var cover = null;
				for(var i = 0; i < response.length; i++)
				{
					if(response[i].indexOf('.flac') != -1)
					{
						albumSongsList.appendChild(createSongDiv(name, response[i]));
					}
					else if(!cover && response[i].indexOf('.jpg') != -1)
					{
						
					}
				}
			}
		};
		x.open('POST', '/fileContents', true);
		x.send(name);
	};

	return albumDiv;
}

function createSongDiv(albumName, song)
{
	var songDiv = document.createElement('li');
	songDiv.textContent = song;

	songDiv.onclick = function()
	{
		//we need to enqueue this
		var queueElement = document.createElement('div');
		queueElement.className = 'encoding';
		queueElement.textContent = song;
		document.getElementById('queue').appendChild(queueElement);

		//send encode request to the server
		var x = new XMLHttpRequest();
		x.onreadystatechange = function()
		{
		  //when the server has reported the file resy
			if(x.readyState == 4)
			{
				queueElement.className = 'ready';
                //execute any callbacks this queue element may have
                //(this is not currenty used, delete if needed)
				if(queueElement.cb)
				{
					queueElement.cb();
				}
                //check if there is currently a song playing
				if(!document.getElementById('player').src || document.getElementById('player').src.indexOf('.mp3') == -1)
				{
					var songElement = document.getElementById('queue').children[0];
				    //if this is the first thing, we start it
                    if(songElement === queueElement)
                    {
                        playSongElement(songElement);
                    }
				}
			}
		};
		x.open('POST', '/extractAndEncode', true);
		x.send(JSON.stringify({file: song, rarFile: albumName}));

		//click the song in the queue to remove it
		queueElement.onclick = function()
		{
			//clear the handler for the xmlhttprequest
			x.onReadyStateChange = function(){};
			//destroy the dom element
			this.parentNode.removeChild(this);
		};
	};

	return songDiv;
}

function playSongElement(songElement)
{
    //update the title and label
    var name = songElement.textContent;
    var titleName = name.substring(name.lastIndexOf('/') + 1, name.lastIndexOf('.')) + " - Flandre Music Server";
    document.title = titleName.substring(titleName.indexOf(' '));
	document.getElementById('nowPlaying').textContent = name;
	playSong(name);
	songElement.parentNode.removeChild(songElement);
}

function playSong(name)
{
	var mp3path = '/mp3/' + name.substring(name.lastIndexOf('/') + 1, name.lastIndexOf('.')) + '.mp3';
	var player = document.getElementById('player');
	player.src = mp3path;
	player.play();
}
