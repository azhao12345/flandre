/* jshint node: true */
"use strict";

var fs = require('fs');
var spawn = require('child_process').spawn;

var rar = function(filename)
{
	var thisrar = this;
	this.filename = filename;
	var unrar = spawn('unrar', ['vb', filename]);
	var contents = '';
	unrar.stdout.on('data', function(data)
	{
		contents += data.toString();
	});

	unrar.stdout.on('close', function()
	{
		thisrar.contents = contents.split('\n');
		if(thisrar.cb)
		{
			thisrar.cb(thisrar.contents);
		}
	});

	unrar.stderr.on('data', function(data)
	{
		console.log(data.toString());
	});
};

module.exports = rar;

rar.prototype.getFiles = function(cb)
{
	if(this.contents)
	{
		cb(this.contents);
	}
	else
	{
		this.cb = cb;
	}
};

rar.prototype.extractFile = function(file, destination, cb)
{
	var unrar = spawn('unrar', ['e', '-o-', this.filename, file, destination]);
	unrar.stdout.on('data', function(data)
	{
		console.log(data.toString());
	});
	unrar.stderr.on('data', function(data)
	{
		console.log(data.toString());
	});
	unrar.on('exit', function(code)
	{
		cb();
	});
};
