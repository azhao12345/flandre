Flandre
=======
flan streaming service
フラのストリーミングサビス

Flandre is a minimal streaming web player designed to stream your library to any modern web browser
フランドはあなたの音楽文からにんいなウエブブラウザまで音楽をストリーミングするのためにミニマルなストリーミングウエブプレア。

Setup
-----
Flan requires a recent version of node.js and express.js

Flan music files must be stored in flac format inside rar archives, 1 archive per album.  Output files are stored in /tmp.

Flan is configured in config.js in the main application directory
```
module.exports =
{
  fileDir: [your file location]
  fileRefreshInterval: [time in milliseconds between file list refreshes(optional)]
}
```
